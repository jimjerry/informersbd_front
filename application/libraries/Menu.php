<?php
Class Menu{
        public static function main_menu(){
            
            $menu = array(
                
                0 => array(
                    'controller'        => 'Home',
                    'value'             => 'Home',
                    'page_name'         => 'Home',
                    'drop_down_class'   => '',
                    'drop_down_menu'    => '',
                ),                
                                
                
                
                1 => array(
                    'controller'        => 'About',
                    'value'             => 'About Us',
                    'page_name'         => 'About',
                    'drop_down_class'   => '',
                    'drop_down_menu'    => '',
                ), 
                
                
                
                2 => array(
                    'controller'      => 'Services',
                    'value'           => 'Services',
                    'page_name'       => 'Services',
                    'drop_down_class' => 'drop-down',                    
                    'drop_down_menu'  => array(                        
                            0 => array(
                                'controller' => 'web_design',
                                'value'      => 'Web Design'
                            ),
                        
                            1 => array(
                                'controller' => 'web_development',
                                'value'      => 'Web Development'
                            ),  
                        
                           /* 2 => array(
                                'controller' => 'product_management',
                                'value'      => 'Product Management'
                            ),   
                        
                            3 => array(
                                'controller' => 'marketing',
                                'value'      => 'Marketing'
                            ), */
                        
                            4 => array(
                                'controller' => 'graphic_design',
                                'value'      => 'Graphic Design'
                            ),  
                            /* 
                            5 => array(
                                'controller' => 'front_end_development',
                                'value'      => 'Front End Development'
                            ),   
                             */                   
                    ),
                ),
                
                3 => array(
                    'controller'        => 'Portfolio',
                    'value'             => 'Portfolio',
                    'page_name'       => 'Portfolio',
                    'drop_down_class'   => '',
                    'drop_down_menu'    => '',
                ), 
                
                
                
                
                4 => array(
                    'controller'        => 'Contact',
                    'value'             => 'Contact Us',
                    'page_name'       => 'Contact',
                    'drop_down_class'   => '',
                    'drop_down_menu'    => '',
                ), 
            );
            return $menu;
        }
        
        public static function service_menus($page_name){
            
            $ci =& get_instance();
            $service_menus = self::main_menu();
            $data['menu'] = $service_menus[2]['drop_down_menu'];
            $data['page_name']= $page_name;
            $ci->load->view('template/service_menu_html', $data);
            
            
        }
        
        public static function social_menus(){
            
            $social_menus = array(
                
                0 => array(
                    'url' => '#',
                    'class' => 'twitter',
                    'icon'  => 'icofont-twitter'
                ),
                
                1 => array(
                    'url' => 'https://www.facebook.com/donald.jerry.1',
                    'class' => 'facebook',
                    'icon'  => 'icofont-facebook'
                ),
                
                2 => array(
                    'url' => '#',
                    'class' => 'instagram',
                    'icon'  => 'icofont-instagram'
                ),
                
                3 => array(
                    'url' => '#',
                    'class' => 'skype',
                    'icon'  => 'icofont-skype'
                ),
                
                4 => array(
                    'url' => '#',
                    'class' => 'linkedin',
                    'icon'  => 'icofont-linkedin'
                )
                
            );
            
            return $social_menus;
            
        }
        
    }
?>