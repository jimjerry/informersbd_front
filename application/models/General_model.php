<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General_model extends CI_Model {
    
    public function DataInsert($table, $data) {
        $result = $this->db->insert($table, $data);
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }
    
}