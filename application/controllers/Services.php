<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {
        
    function __construct() {
        parent::__construct();        
    } 
    
    public function index(){
        $data['page_name'] = 'Services';        
        $this->load->view('mt/default', $data);
    }    
    
    
    public function web_design(){
        $data['page_name'] = 'web_design';        
        $this->load->view('mt/default', $data);
    }
    
    public function web_development(){
        $data['page_name'] = 'web_development';
        $this->load->view('mt/default', $data);
    }    
    
    public function graphic_design(){
        $data['page_name'] = 'graphic_design';
        $this->load->view('mt/default', $data);
    }    

}