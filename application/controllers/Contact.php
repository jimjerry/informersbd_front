<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {
    
    public function index(){
        
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('subject', 'subject', 'required');
        $this->form_validation->set_rules('message', 'message', 'required');            
        
        
        if($this->form_validation->run()){            
            $name    = $this->input->post('name');
            $email   = $this->input->post('email');
            $subject = $this->input->post('subject');
            $message = $this->input->post('message');
            
            $mail_data = array(
                'name'    =>   $name,
                'email'   =>   $email,
                'subject' =>   $subject,
                'message' =>   $message
            );
            
            $this->load->model('General_model');
            $result = $this->General_model->DataInsert('contact_form', $mail_data);
            if($result){
                $data['success_msg'] = "<div class='sent-message'>Your message has been sent. Thank you!</div>";
                $data['page_name'] = 'Contact';
                $this->load->view('mt/default', $data);
            }
        }else{        
            $data['page_name'] = 'Contact';
            $this->load->view('mt/default', $data);
        }
    }
    
}