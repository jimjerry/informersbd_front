<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portfolio extends CI_Controller {
    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $data['page_name'] = 'Portfolio';
        $this->load->view('mt/default', $data);
    }
}

?>