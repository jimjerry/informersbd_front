<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {
    
    public function index(){
        $data['page_name'] = 'About';
        $this->load->view('mt/default', $data);
    }
    
}