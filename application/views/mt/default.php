<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>Informers BD</title>
        <meta content="" name="descriptison">
        <meta content="" name="keywords">

        <!-- Favicons -->
        <link href="<?= base_url(); ?>assets/img/favicon.png" rel="icon">
        <link href="<?= base_url(); ?>assets/img/apple-touch-icon.png" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
        <!-- Vendor CSS Files -->
        <link href="<?= base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= base_url(); ?>assets/vendor/icofont/icofont.min.css" rel="stylesheet">
        <link href="<?= base_url(); ?>assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
        <link href="<?= base_url(); ?>assets/vendor/animate.css/animate.min.css" rel="stylesheet">
        <link href="<?= base_url(); ?>assets/vendor/venobox/venobox.css" rel="stylesheet">
        <!--<link href="assets/vendor/aos/aos.css" rel="stylesheet">-->

        <!-- Template Main CSS File -->
        <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet">


    </head>

    <body>

        <!-- ======= Top Bar ======= -->
        <section id="topbar" class="d-none d-lg-block">
            <div class="container clearfix">
                <div class="contact-info float-left">
                    <i class="icofont-envelope"></i><a href="mailto:contact@informersbd.com">contact@informersbd.com</a>
                    <i class="icofont-phone"></i> +1 5589 55488 55
                </div>
                <div class="social-links float-right">
                    
                    <?php
                    
                        $social_menu = Menu::social_menus();
                        foreach ($social_menu as $sm){
                            if($sm['url'] != ''){
                            
                    ?>
                        <a href="<?= $sm['url']?>" class="<?= $sm['class']?>"><i class="<?= $sm['icon']?>"></i></a>
                        <?php }} ?>
                    
                </div>
            </div>
        </section>

        <!-- ======= Header ======= -->
        <header id="header">
            <div class="container">

                <div class="logo float-left">
                    <h1 class="text-light"><a href="<?= base_url(); ?>"><span>Informers BD</span></a></h1>
                    <!-- Uncomment below if you prefer to use an image logo -->
                    <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
                </div>

                <nav class="nav-menu float-right d-none d-lg-block">                   

                    <ul>
                        <?php
                        //$this->load->library('Menu');
                        $menu = Menu::main_menu();
                        foreach ($menu as $main_m) { ?>                            

                        <li class="<?=($page_name == $main_m['page_name'])? "active " : "";?><?= isset($main_m['drop_down_class']) ? $main_m['drop_down_class'] : "" ?>">
                                <a href="<?= base_url() . $main_m['controller'] ?>"><?= $main_m['value']; ?></a>
                                <?php
                                $r = $main_m['drop_down_menu'];

                                if (!empty($main_m['drop_down_menu'])) { ?>
                                    <ul>
                                        <?php foreach ($r as $s) { ?>
                                          <li>
                                              <a href="<?= base_url().$main_m['controller'] ?>/<?php print_r($s['controller']); ?>"><?php print_r($s['value']); ?></a>
                                          </li>
                                        <?php } ?>
                                    </ul>
                                    <?php } ?>
                            </li>

                            <?php } ?>
                    </ul>

                </nav><!-- .nav-menu -->

            </div>
        </header><!-- End Header -->

<?php
if (isset($page_name) && $page_name == "Home") {
    $this->load->view("template/home");
} elseif (isset($page_name) && $page_name == "Contact") {
    $this->load->view("template/contact");
} elseif (isset($page_name) && $page_name == "About") {
    $this->load->view("template/about");
}

elseif (isset($page_name) && $page_name == "Services" || $page_name == "web_design" || $page_name == "web_development" || $page_name == "graphic_design") {
    $this->load->view("template/services");
}

elseif(isset($page_name) && $page_name == "Portfolio"){
    $this->load->view("template/portfolio");
}

else {
    $this->load->view("template/home");
}
?>

        <!-- ======= Footer ======= -->
        <footer id="footer">
            <div class="footer-top">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-3 col-md-6 footer-info">
                            <h3>INFORMERS BD</h3>
                            <p>
                                A108 Adam Street <br>
                                NY 535022, USA<br><br>
                                <strong>Phone:</strong> +1 5589 55488 55<br>
                                <strong>Email:</strong> info@example.com<br>
                            </p>
                            <div class="social-links mt-3">
                                <?php
                    
                                    $social_menu = Menu::social_menus();
                                    foreach ($social_menu as $sm){
                                        if($sm['url'] != ''){
                                            
                                ?>
                                <a href="<?= $sm['url']?>" class="<?= $sm['class']?>"><i class="<?= $sm['icon']?>"></i></a>
                                <?php }} ?>
                                
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-6 footer-links">
                            <h4>Useful Links</h4>
                            <ul>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
                            </ul>
                        </div>

                        <div class="col-lg-3 col-md-6 footer-links">
                            <h4>Our Services</h4>
                            <ul>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
                                <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
                            </ul>
                        </div>

                        <div class="col-lg-4 col-md-6 footer-newsletter">
                            <h4>Our Newsletter</h4>
                            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
                            <form action="" method="post">
                                <input type="email" name="email"><input type="submit" value="Subscribe">
                            </form>

                        </div>

                    </div>
                </div>
            </div>

            <div class="container">
                <div class="copyright">
                    &copy; Copyright <strong><span>Informers BD</span></strong>. All Rights Reserved
                </div>                
            </div>
        </footer><!-- End Footer -->

        <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

        <!-- Vendor JS Files -->
        <script src="<?= base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>
        <script src="<?= base_url(); ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="<?= base_url(); ?>assets/vendor/jquery.easing/jquery.easing.min.js"></script>
        <script src="<?= base_url(); ?>assets/vendor/php-email-form/validate.js"></script>
        <script src="<?= base_url(); ?>assets/vendor/jquery-sticky/jquery.sticky.js"></script>
        <script src="<?= base_url(); ?>assets/vendor/venobox/venobox.min.js"></script>
        <script src="<?= base_url(); ?>assets/vendor/waypoints/jquery.waypoints.min.js"></script>
        <script src="<?= base_url(); ?>assets/vendor/counterup/counterup.min.js"></script>
        <script src="<?= base_url(); ?>assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
        <script src="<?= base_url(); ?>assets/vendor/aos/aos.js"></script>

        <!-- Template Main JS File -->
        <script src="<?= base_url(); ?>assets/js/main.js"></script>

    </body>

</html>