<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="title">Web Design</h4>
            </div>
            <div class="col-lg-4">             
                <ul class="services_ul_li nav navbar-nav">
                    <li><i class="icofont-checked"></i> A Strong, but Limited, Color Palette</li>
                    <li><i class="icofont-checked"></i> Plenty of White Space</li>
                    <li><i class="icofont-checked"></i> Responsive Hero Images</li>
                    <li><i class="icofont-checked"></i> Valuable Calls to Action</li>
                    <li><i class="icofont-checked"></i> Cohesive Card Designs</li>
                    <li><i class="icofont-checked"></i> High-Quality Product Videos</li>
                    <li><i class="icofont-checked"></i> Clean Backend Coding</li>
                    <li><i class="icofont-checked"></i> Design for the User First</li>
                    <li><i class="icofont-checked"></i> SEO-Boosting Elements</li>
                    <li><i class="icofont-checked"></i> Speed Optimization</li>
                </ul>
            </div>

        </div>
    </div>
</section>