<style type="text/css">
    .form_error{
        color: red;
        font-size: 13px;
    }
</style>
<main id="main">
    <section id="contact" class="contact">
        <div class="container">

            <div class="section-title">
                <h2>Contact Us</h2>
            </div>

            <div class="row">

                <div class="col-lg-6 d-flex align-items-stretch" data-aos="fade-up">
                    <div class="info-box">
                        <i class="bx bx-map"></i>
                        <h3>Our Address</h3>
                        <p>A108 Adam Street, New York, NY 535022</p>
                    </div>
                </div>

                <div class="col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                    <div class="info-box">
                        <i class="bx bx-envelope"></i>
                        <h3>Email Us</h3>
                        <p>info@example.com<br>contact@example.com</p>
                    </div>
                </div>

                <div class="col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
                    <div class="info-box ">
                        <i class="bx bx-phone-call"></i>
                        <h3>Call Us</h3>
                        <p>+1 5589 55488 55<br>+1 6678 254445 41</p>
                    </div>
                </div>

                <div class="col-lg-12" data-aos="fade-up" data-aos-delay="300">
                    <form action="<?= base_url()?>Contact" method="post" role="form" class="php-email-form">
                        <div class="form-row">
                            <div class="col-lg-6 form-group">
                                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                <div id="name_form_error" class="form_error"><?= form_error('name');?></div>
                            </div>
                            <div class="col-lg-6 form-group">
                                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                                <div id="email_form_error" class="form_error"><?= form_error('email');?></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                            <div id="subject_form_error" class="form_error"><?= form_error('subject');?></div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="message" id="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                            <div  id="msg_form_error" class="form_error"><?= form_error('message');?></div>
                        </div>
                        <div class="mb-3">                            
                            
                            <?php
                                if(isset($success_msg)){
                                    echo $success_msg;
                                }
                             ?>
                        </div>
                        <div id="contact_form" class="text-center"><button type="submit">Send Message</button></div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main>

<script type="text/javascript">
    
    $("#contact_form").on("click",function(){var r=$.trim($("#name").val()),e=$.trim($("#email").val()),m=$.trim($("#subject").val()),o=$.trim($("#message").val());if(""==r?$("#name_form_error").html("Name Require"):$("#name_form_error").empty(),""==e?$("#email_form_error").html("Email Require"):$("#email_form_error").empty(),""==m?$("#subject_form_error").html("Subject Require"):$("#subject_form_error").empty(),""==o?$("#msg_form_error").html("Message Require"):$("#msg_form_error").empty(),""==r||""==e||""==m||""==o)return!1});
    
</script>