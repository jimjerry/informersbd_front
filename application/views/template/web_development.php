<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="title">Web Development</h4>
            </div>
            <div class="col-lg-4">             
                <ul class="services_ul_li nav navbar-nav">
                    <li><i class="icofont-checked"></i> Analysis</li>
                    <li><i class="icofont-checked"></i> Design</li>
                    <li><i class="icofont-checked"></i> Implementation</li>
                    <li><i class="icofont-checked"></i> Promotion</li>
                    <li><i class="icofont-checked"></i> Innovation</li>
                    <li><i class="icofont-checked"></i> Planning</li>

                </ul>
            </div>

        </div>
    </div>
</section>