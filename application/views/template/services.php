<style type="text/css">
    section#service_nav{
        padding: 0;
        height: 52px;
        width: 100%;
        background: #428bca;
    }
    
    .bg-primary{
        background: #428bca!important ;
    }
    #service_m{
        padding: 0;
    }
    
    .service_page{
        height: 200px;
        width: 100%;
        background: url('<?= base_url()?>assets/img/slide/slide-1.jpg') no-repeat;
        background-size: cover;        
    }
    
    .ab-2{
        height: 200px;
        width: 100%;
        background: rgba(13, 30, 45, 0.6); 
        padding-top: 72px;
    }
    
    h2 {
        color: #fff;
        margin-bottom: 30px;
        font-size: 48px;
        font-weight: 900;
    }
    p{
      color: #fff;
    }
    
    #service_nav ul{
        line-height: 30px;
    }
    #service_nav ul li{
        height: 52px;
        line-height: 40px;
        font-weight: bold;
        /* padding: 0 10px; */
    }
    #service_nav ul li a{
        color: #fff;
    }
    #service_nav ul li.active, #service_nav ul li a:hover{
        height: 52px;
        border-bottom : 2px solid #fff;
        background: #3f99e6;
    }
    #service_nav ul li a{
        color: #fff;
        text-transform: uppercase;
        font-size: 12px;
        padding: 6px 22px;
    }
    
    
    .loader_img{
        width: 100%;
        height: 220px;
        text-align: center;
        display: none;
    }
    .loader_img img{
        width: 40px;
        height: auto;
        margin-top: 9%;
    }
    
    .services_ul_li li{
        padding: 10px 0;
    }
    
    .navbar-light .navbar-toggler{
        color: #fff;
        border: none;
    }
</style>


<div class="service_page">
    <div class="ab-2">
                
        <div class="carousel-content container">
            <h2 class="animated fadeInDown">Services</span></h2>
        </div>
        
    </div>
</div>

<?php
    echo Menu::service_menus($page_name);
    
    if(isset($page_name) && $page_name == "Services"){
        $this->load->view('template/all_services');
    }
    elseif(isset($page_name) && $page_name == "web_design"){
        $this->load->view('template/web_design');
    }
    elseif(isset($page_name) && $page_name == "web_development"){
        $this->load->view('template/web_development');
    }
    elseif(isset($page_name) && $page_name == "graphic_design"){
        $this->load->view('template/graphic_design');
    }
    
    else{
        $this->load->view('template/all_services');
    }
?>

