<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="title">Graphic Design</h4>
            </div>
            <div class="col-lg-4">             
                <ul class="services_ul_li nav navbar-nav">
                    <li><i class="icofont-checked"></i> Cut-out of images</li>
                    <li><i class="icofont-checked"></i> Retouching</li>
                    <li><i class="icofont-checked"></i> Colour correction</li>
                    <li><i class="icofont-checked"></i> Re-coloring of products</li>
                    <li><i class="icofont-checked"></i> Special tasks</li>
                    <li><i class="icofont-checked"></i> Reasonable Prices</li>                    
                    <li><i class="icofont-checked"></i> Image Manipulation</li>
                    <li><i class="icofont-checked"></i> Headshot Retouch</li>
                    <li><i class="icofont-checked"></i> Logo Design</li>
                </ul>
            </div>

        </div>
    </div>
</section>