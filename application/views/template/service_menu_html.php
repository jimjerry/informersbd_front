<section id='service_m' class='navbar navbar-expand-lg navbar-light bg-primary'>
    <div class='container'>                
        <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#service_nav' aria-controls='service_nav' aria-expanded='false' aria-label='Toggle navigation'>
            <i class="icofont-navigation-menu"></i> Our All Services
        </button>
        <div class='collapse navbar-collapse' id='service_nav'>
            <ul class='navbar-nav'>
                <li class='nav-item <?= ($page_name == 'Services')? 'active' : '' ?>'>
                    <a id='all' class='nav-link' href='<?=  base_url()?>Services'>All</a>
                </li>
                
                <?php foreach($menu as $sm){?>
                    <li class='nav-item <?= ($sm['controller'] == $page_name)? 'active' : '' ?>'>
                        <a id='all' class='nav-link' href='<?= base_url() ?>Services/<?= $sm['controller']; ?>'><?= $sm['value'] ?></a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</section>