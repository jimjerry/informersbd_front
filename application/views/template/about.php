<style type="text/css">
    .about_us_section{
        height: 600px;
        width: 100%;
        background: url('assets/img/slide/slide-1.jpg') no-repeat;
        background-size: cover;
    }
    .ab-2{
        height: 600px;
        width: 100%;
        background: #00000087; 
        padding-top: 174px;
    }
    h2 {
        color: #fff;
        margin-bottom: 30px;
        font-size: 48px;
        font-weight: 900;
    }
    p{
      color: #fff;
    }
    
    .btn-get-started {
        font-family: "Raleway", sans-serif;
        font-weight: 500;
        font-size: 14px;
        letter-spacing: 1px;
        display: inline-block;
        padding: 12px 32px;
        border-radius: 5px;
        transition: 0.5s;
        line-height: 1;
        margin: 10px;
        color: #fff;
        -webkit-animation-delay: 0.8s;
        animation-delay: 0.8s;
        border: 0;
        background: #428bca;
    }
</style>   


<div class="about_us_section">
    <div class="ab-2">
                
        <div class="carousel-content container">
            <h2 class="animated fadeInDown">About Us</span></h2>
        </div>
        
    </div>
</div>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                
            </div>
            <div class="col-lg-6">
                Welcome to <b>Informers BD</b>, your number one source for all things Web Design, Web Development, Product Management, Marketing, Graphic Design, Front End Development. We're dedicated to giving you the very best of website, with a focus on Security, Cross browser compatibility, 24/7 support. Founded in 2020 by Donald Jerry has come a long way from its beginnings. We now serve customers all worldwide.

We hope you enjoy our products as much as we enjoy offering them to you. If you have any questions or comments, please don't hesitate to contact us.

Sincerely,
Name, [title, ie: CEO, Founder, etc.]
            </div>
        </div>
    </div>
</section>