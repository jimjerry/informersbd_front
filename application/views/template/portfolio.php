<style type="text/css">
    section#service_nav{
        padding: 0;
        height: 52px;
        width: 100%;
        background: #428bca;
    }
    
    .bg-primary{
        background: #428bca!important ;
    }
    #service_m{
        padding: 0;
    }
    
    .portfolio_page{
        height: 200px;
        width: 100%;
        background: url('<?= base_url()?>assets/img/slide/slide-1.jpg') no-repeat;
        background-size: cover;        
    }
    
    .ab-2{
        height: 200px;
        width: 100%;
        background: rgba(13, 30, 45, 0.6); 
        padding-top: 72px;
    }
    
    h2 {
        color: #fff;
        margin-bottom: 30px;
        font-size: 48px;
        font-weight: 900;
    }
    #portfolio p{
      color: #000;
    }
    
    
</style>
<div class="portfolio_page">
    <div class="ab-2">
                
        <div class="carousel-content container">
            <h2 class="animated fadeInDown">Portfolio</span></h2>
        </div>
        
    </div>
</div>


<section id="portfolio" class="portfolio section-bg">
                <div class="container" data-aos="fade-up" data-aos-delay="100">

                    <div class="section-title">
                        <h2>Our Works</h2>
                        <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <ul id="portfolio-flters">
                                <li data-filter="*" class="filter-active">All</li>
                                <li data-filter=".filter-app">App</li>
                                <li data-filter=".filter-card">Card</li>
                                <li data-filter=".filter-web">Web</li>
                            </ul>
                        </div>
                    </div>

                    <div class="row portfolio-container">

                        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                            <div class="portfolio-wrap">
                                <img src="assets/img/portfolio/advancedinsight.jpeg" class="img-fluid" alt="">
                                <div class="portfolio-info">
                                    <h4>Advanced Insight</h4>
                                    <p>Advanced Insight</p>
                                    <div class="portfolio-links">
                                        <a href="assets/img/portfolio/advancedinsight_lg.jpeg" data-gall="portfolioGallery" class="venobox" title="App 1"><i class="icofont-eye"></i></a>
                                        <a target="_blank" href="http://advancedinsight.com.bd/" title="More Details"><i class="icofont-external-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                            <div class="portfolio-wrap">
                                <img src="assets/img/portfolio/graphicsranger.jpeg" class="img-fluid" alt="">
                                <div class="portfolio-info">
                                    <h4>Web 3</h4>
                                    <p>Web</p>
                                    <div class="portfolio-links">
                                        <a href="assets/img/portfolio/graphicsranger_lg.jpeg" data-gall="portfolioGallery" class="venobox" title="Web 3"><i class="icofont-eye"></i></a>
                                        <a target="_blank"href="http://graphicsranger.com/" title="More Details"><i class="icofont-external-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                            <div class="portfolio-wrap">
                                <img src="assets/img/portfolio/aidgraphic.jpeg" class="img-fluid" alt="">
                                <div class="portfolio-info">
                                    <h4>App 2</h4>
                                    <p>App</p>
                                    <div class="portfolio-links">
                                        <a href="assets/img/portfolio/aidgraphic_lg.jpeg" data-gall="portfolioGallery" class="venobox" title="App 2"><i class="icofont-eye"></i></a>
                                        <a target="_blank"href="https://aidgraphic.com/" title="More Details"><i class="icofont-external-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                            <div class="portfolio-wrap">
                                <img src="assets/img/portfolio/graphic_design.jpg" class="img-fluid" alt="">
                                <div class="portfolio-info">
                                    <h4>Graphic Design</h4>
                                    <p>Retouch and Cutting</p>
                                    <div class="portfolio-links">
                                        <a href="assets/img/portfolio/graphic_design_sm.jpeg" data-gall="portfolioGallery" class="venobox" title="Card 2"><i class="icofont-eye"></i></a>
                                        <a href="#" title="More Details"><i class="icofont-external-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                            <div class="portfolio-wrap">
                                <img src="assets/img/portfolio/portfolio-5.jpg" class="img-fluid" alt="">
                                <div class="portfolio-info">
                                    <h4>Web 2</h4>
                                    <p>Web</p>
                                    <div class="portfolio-links">
                                        <a href="assets/img/portfolio/portfolio-5.jpg" data-gall="portfolioGallery" class="venobox" title="Web 2"><i class="icofont-eye"></i></a>
                                        <a href="#" title="More Details"><i class="icofont-external-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                            <div class="portfolio-wrap">
                                <img src="assets/img/portfolio/portfolio-6.jpg" class="img-fluid" alt="">
                                <div class="portfolio-info">
                                    <h4>App 3</h4>
                                    <p>App</p>
                                    <div class="portfolio-links">
                                        <a href="assets/img/portfolio/portfolio-6.jpg" data-gall="portfolioGallery" class="venobox" title="App 3"><i class="icofont-eye"></i></a>
                                        <a href="#" title="More Details"><i class="icofont-external-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                            <div class="portfolio-wrap">
                                <img src="assets/img/portfolio/portfolio-7.jpg" class="img-fluid" alt="">
                                <div class="portfolio-info">
                                    <h4>Card 1</h4>
                                    <p>Card</p>
                                    <div class="portfolio-links">
                                        <a href="assets/img/portfolio/portfolio-7.jpg" data-gall="portfolioGallery" class="venobox" title="Card 1"><i class="icofont-eye"></i></a>
                                        <a href="#" title="More Details"><i class="icofont-external-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                            <div class="portfolio-wrap">
                                <img src="assets/img/portfolio/portfolio-8.jpg" class="img-fluid" alt="">
                                <div class="portfolio-info">
                                    <h4>Card 3</h4>
                                    <p>Card</p>
                                    <div class="portfolio-links">
                                        <a href="assets/img/portfolio/portfolio-8.jpg" data-gall="portfolioGallery" class="venobox" title="Card 3"><i class="icofont-eye"></i></a>
                                        <a href="#" title="More Details"><i class="icofont-external-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                            <div class="portfolio-wrap">
                                <img src="assets/img/portfolio/portfolio-9.jpg" class="img-fluid" alt="">
                                <div class="portfolio-info">
                                    <h4>Web 3</h4>
                                    <p>Web</p>
                                    <div class="portfolio-links">
                                        <a href="assets/img/portfolio/portfolio-9.jpg" data-gall="portfolioGallery" class="venobox" title="Web 3"><i class="icofont-eye"></i></a>
                                        <a href="#" title="More Details"><i class="icofont-external-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </section>